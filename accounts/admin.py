from django.contrib import admin
from apps.accounts.models import User


class UserAdmin(admin.ModelAdmin):
    list_display = ('username', 'first_name', 'last_name', 'email', 'type', 'is_active', 'is_staff','is_superuser')
    list_filter = ('type', 'is_staff', 'is_superuser')


admin.site.register(User, UserAdmin)