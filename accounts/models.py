from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    class Type(models.TextChoices):
        ADMIN = 'admin', 'Admin'
        USER = 'user', 'User'

    type = models.CharField(max_length=10, choices=Type.choices, default=Type,verbose_name='Тип')

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователь'

    def __str__(self):
        return f'{self.first_name} {self.last_name}'
